import React, { useState } from 'react'
import { useFormik } from 'formik';
import * as Yup from 'yup';

const Home = () => {

    const [send, setSend] = useState(false)
    const [name, setName] = useState('')

    const phoneRegExp = /^(?:(?:\+|00)33|0)\s*[1-9](?:[\s.-]*\d{2}){4}$/
    const formik = useFormik({
        initialValues: {
            firstName: '',
            lastName: '',
            email: '',
            birthday: '',
            mobile: ''
        },
        validationSchema: Yup.object({
            lastName: Yup.string()
                .min(2, 'Le nom est trop court')
                .required('Nom requis'),
            firstName: Yup.string()
                .min(2, 'Le prénom est trop court')
                .required('Prénom requis'),
            email: Yup.string()
                .email('Adresse mail invalide')
                .required('Email requis'),
            birthday: Yup.date()
                .required('Date de naissance requise'),
            mobile: Yup.string().matches(phoneRegExp, 'Numéro de téléphone invalide')
                .required('Votre numéro est requis'),
        }),
        onSubmit: values => {
            setSend(true)
            setName(values.firstName)
        },
    });

    return (
        <div className="w-full flex flex-col items-center justify-center">

            <h1 className="text-white mt-16 mb-10 uppercase font-serif text-4xl">ColBR</h1>
            {
                send ? (
                    <h6 className="text-white text-4xl">Merci beaucoup <span className="capitalize">{name}</span> pour votre confiance !</h6>
                ) : (
                    <form onSubmit={formik.handleSubmit} className="bg-white flex flex-col items-center rounded p-5 w-1/3" autoComplete="nope">
                        <div className="flex flex-col mb-3">
                            <label htmlFor="lastName">Nom</label>
                            <input
                                id="lastName"
                                name="lastName"
                                type="text"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.lastName}
                                className="shadow appearance-none border rounded w-72 focus-within:outline-none p-1"
                                autoComplete="lastName"
                            />
                            {formik.touched.lastName && formik.errors.lastName ? <div className="text-red-500">{formik.errors.lastName}</div> : null}
                        </div>

                        <div className="flex flex-col mb-3">
                            <label htmlFor="firstName">Prénom</label>
                            <input
                                className="shadow appearance-none border rounded w-72 focus-within:outline-none p-1"

                                id="firstName"
                                name="firstName"
                                type="text"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.firstName}
                                autoComplete="nope"
                            />
                            {formik.touched.firstName && formik.errors.firstName ? (
                                <div className="text-red-500">{formik.errors.firstName}</div>
                            ) : null}
                        </div>

                        <div className="flex flex-col mb-3">
                            <label htmlFor="email">Email :</label>
                            <input
                                id="email"
                                name="email"
                                type="email"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.email}
                                className="shadow appearance-none border rounded w-72 focus-within:outline-none p-1"
                                autoComplete="nope"
                            />
                            {formik.touched.email && formik.errors.email ? <div className="text-red-500">{formik.errors.email}</div> : null}
                        </div>

                        <div className="flex flex-col mb-3">
                            <label htmlFor="birthday">Date d'anniversaire :</label>
                            <input
                                id="birthday"
                                name="birthday"
                                type="date"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.birthday}
                                className="shadow appearance-none border rounded w-72 focus-within:outline-none p-1"
                            />
                            {formik.touched.birthday && formik.errors.birthday ? <div className="text-red-500">{formik.errors.birthday}</div> : null}
                        </div>

                        <div className="flex flex-col mb-3">
                            <label htmlFor="mobile">Téléphone :</label>
                            <input
                                id="mobile"
                                name="mobile"
                                type="tel"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.mobile}
                                className="shadow appearance-none border rounded w-72 focus-within:outline-none p-1"
                                autoComplete="nope"
                            />
                            {formik.touched.mobile && formik.errors.mobile ? <div className="text-red-500">{formik.errors.mobile}</div> : null}
                        </div>
                        <div className="flex flex-col w-72 items-start">
                            <button type="submit" className="bg-green-400 outline-none focus-within:outline-none mt-4 w-20 rounded text-white">Envoyer</button>
                        </div>
                    </form>
                )
            }
        </div>
    )
}

export default Home
